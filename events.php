<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/style.css">
<link rel="stylesheet" href="./assets/css/events-style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>LEADING THE WAY IN BUSINESS</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1><span>View All SGV Events</span><br/> and Seminars</h1></div>
                            <!-- <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="office__location">
        <div class="container">
            <div class="row">
                <div class="office__location__container">
                    <div class="events__container--sub-title office--sub--title">
                        <span>
                            <input type="text" class="input_form events__container--search--input" placeholder="Search Events and Seminars">
                        <img src="./assets/images/search-icon.png" alt="">
                        </span>
                    </div>
                    <div class="office__location__container--office--row">
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Corporate Governance Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Corporate Governance Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-divider"></div>

    <div class="event__older__container">
        <div class="container">
            <div class="row">
                <div class="event__older__container--row">
                    <span>
                        Older
                    </span>
                    <div>
                    <img src="./assets/images/chevron-right-yellow.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="event__get__in__touch--icon">
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                                <p class="event__get__in__touch--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                            <p class="event__get__in__touch--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                                <p class="event__get__in__touch--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
