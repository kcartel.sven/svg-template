<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/alumni-style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>LEADING THE WAY IN BUSINESS</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1><span>View All SGV Events</span><br/> and Seminars</h1></div>
                            <!-- <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="alumni__news__events">
        <div class="container">
            <div class="row">
                <div class="alumni__news__events__container">
                    <div class="alumni__news__events__container--header--content--flex">
                        <div class="header__content">
                            <div class="header__content--sub-title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>HOME</span>
                                </div>
                            </div>
                            <div class="header__content--main--title">
                                <div class="contact__form__container--main--title"><h2><span>View All SGV Alumni</span><br/> News and Events</h2></div>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--select--option">
                            <select name="" id="">
                                <option value="">Select Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="alumni__news__events__container--office--row">
                        <div class="alumni__news__events__container--office--content">
                            <h3>Pre-Homecoming Party For All</h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Know more
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Alumni Corporate Governance </h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Alumni Donates 1M In Cash</h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Pre-Homecoming Party For All</h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Know more
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Alumni Corporate Governance </h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Alumni Donates 1M In Cash</h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Pre-Homecoming Party For All</h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Know more
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Alumni Corporate Governance </h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="alumni__news__events__container--office--content">
                            <h3>Alumni Donates 1M In Cash</h3>
                            <div class="alumni__news__events__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <div class="alumni__news__events__container--sub-title">
                                    <div class="alumni__news__events__container--sub--title-divider header__divider"></div>
                                    <div class="alumni__news__events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alumni__news__events__container--office--content--address">
                                <span class="alumni__news__events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-divider"></div>
                <div class="alumni__older__container">
                    <div class="container">
                        <div class="row">
                            <div class="alumni__older__container--row">
                                <span>
                                    Older
                                </span>
                                <div>
                                    <img src="./assets/images/chevron-right-yellow.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="svg__alumni__family__container">
        <div class="container">
            <div class="row">
                <div class="svg__alumni__family__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Be part of</span> <br/>The SGV Alumni Family</h2></div>
                        </div>
                    </div>
                    <div class="svg__alumni__family__container--button--row">
                            <span class="send__message__button">
                                Register Now
                                <img src="./assets/images/chevron-right1.png" alt="">
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="event__get__in__touch--icon">
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                                <p class="event__get__in__touch--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                            <p class="event__get__in__touch--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                                <p class="event__get__in__touch--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
