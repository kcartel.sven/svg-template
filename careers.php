<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/careers-style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>WE VALUE TRUST & INTEGRITY IN EVERYTHING WE DO</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1><span>Be part of</span><br/>The SGV Family</h1></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="careers__first__container">
        <!-- <div class="container"> -->
            <!-- <div class="row"> -->
                <!-- <div class="careers__first__container--row"> -->
                    <div class="careers__first__container--row--left">
                    </div>
                    <div class="careers__first__container--row--right">
                        <img src="./assets/images/quote-title.jpg" alt="">
                        <p class="careers__first__container--row--right--title">We bring extraordinary people, like you, together to build a better working world.</p>
                        <div >
                            <p class="careers__first__container--row--right--description">
                            We’re unicorn makers and bot builders. Change agents and cyber gurus. 
                            Performance improvers and problem solvers. Data scientists and growth hackers. 
                            Risk managers and confidence builders. We’re 284,000 global perspectives ready 
                            to welcome yours. Here at EY, you’ll build a legacy. You’ll grow your skillset, and 
                            expand your mindset. You’ll focus your intelligence and imagination on the most 
                            crucial, complex issues facing business, government and society today.
                            </p>
                        </div>
                    </div>
                <!-- </div> -->
            <!-- </div>
        </div> -->
    </div>

    <div class="graduate__candidates">
        <div class="container">
            <div class="row">
                <div class="graduate__candidates__container">
                    <div class="graduate__candidates__container--row">
                        <div class="graduate__candidates__container--sub-title office--sub--title">
                            <span>
                                <input type="text" class="input_form graduate__candidates__container--search--input" placeholder="Search Graduate Careers">
                            <img src="./assets/images/search-icon.png" alt="">
                            </span>
                        </div>
                        <div class="careers__first__container--graduate--candidates--content--first">
                            <div class="header__content--sub-title office--sub--title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>REACH OUT TO US</span>
                                </div>
                            </div>
                            <div class="graduate__candidates__container--main--title--row">
                                <div class="graduate__candidates__container--main--title"><h2><span>Graduate </span> Candidates</h2></div>
                            </div>
                        </div>
                    </div>
                    <div class="graduate__candidates__container--office--row">
                        <div class="graduate__candidates__container--office--content">
                            <h3>SGV Advisory Associate - Performance Improvement</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>Resource Management Admin (Office Staff)</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>Management Engineer - Entry Level - Staff I - Associate</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>SGV Advisory Associate - Performance Improvement</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>Resource Management Admin (Office Staff)</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>Management Engineer - Entry Level - Staff I - Associate</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>SGV Advisory Associate - Performance Improvement</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>Resource Management Admin (Office Staff)</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="graduate__candidates__container--office--content">
                            <h3>Management Engineer - Entry Level - Staff I - Associate</h3>
                            <div class="graduate__candidates__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="graduate__candidates__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="graduate__candidates__container--office--content--address">
                                <span class="graduate__candidates__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="graduate__candidates__container--button graduate__candidates__container--graduate--button">
                        <span class="graduate__candidates__container--graduate--button send__message__button">
                        Go to EY Job Page
                            <img src="./assets/images/chevron-right2.png" alt="">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="experienced__professional__container">
        <div class="container">
            <div class="row">
                <div class="experienced__professional__container__container">
                    <div class="experienced__professional__container__container--row">
                        <div >
                            <div class="header__content--sub-title office--sub--title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>REACH OUT TO US</span>
                                </div>
                            </div>
                            <div class="experienced__professional__container__container--main--title--row">
                                <div class="experienced__professional__container__container--main--title"><h2><span>Experienced Professional
 </span> Candidates</h2></div>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--sub-title office--sub--title">
                            <span>
                                <input type="text" class="input_form experienced__professional__container__container--search--input" placeholder="Search Experience Professional Careers">
                            <img src="./assets/images/search-icon.png" alt="">
                            </span>
                        </div>
                    </div>
                    <div class="experienced__professional__container__container--office--row">
                        <div class="experienced__professional__container__container--office--content">
                            <h3>SGV Advisory Associate - Performance Improvement</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>Resource Management Admin (Office Staff)</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>Management Engineer - Entry Level - Staff I - Associate</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>SGV Advisory Associate - Performance Improvement</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>Resource Management Admin (Office Staff)</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>Management Engineer - Entry Level - Staff I - Associate</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>SGV Advisory Associate - Performance Improvement</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>Resource Management Admin (Office Staff)</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="experienced__professional__container__container--office--content">
                            <h3>Management Engineer - Entry Level - Staff I - Associate</h3>
                            <div class="experienced__professional__container__container--office--content--address">
                                <p>Location: Metro Manila, PH
                                </p>
                                <span>
                                    Category: Advisory
                                </span>
                            </div>
                            <div class="experienced__professional__container__container--office--content--contact">
                                <p>Currently, we are looking for new associates
to fill Performance Improvement service line 
in the following areas:</p>
                            </div>
                            <div class="experienced__professional__container__container--office--content--address">
                                <span class="experienced__professional__container__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right-blue.png" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="experienced__professional__container__container--button experienced__professional__container__container--graduate--button">
                        <span class="experienced__professional__container__container--graduate--button send__message__button">
                        Go to EY Job Page
                            <img src="./assets/images/chevron-right2.png" alt="">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="careers__contact__us__container">
        <div class="container">
            <div class="row">
                <div class="careers__contact__us__container--row">
                    <div class="careers__contact__us__container--content--first">
                        <div class="header__content--sub-title office--sub--title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>GET IN TOUCH</span>
                            </div>
                            <div class="header__divider"></div>
                        </div>
                        <div class="careers__contact__us__container--main--title--row">
                            <div class="careers__contact__us__container--main--title"><h2>ContactUs </h2></div>
                        </div>
                    </div>
                </div>
                <div class="careers__contact__us__container--icon">
                    <div class="careers__contact__us__container--icon--container">
                        <div class="careers__contact__us__container--icon--image">
                            <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                        </div>
                        <div class="careers__contact__us__container--icon--description">
                            <p>6760 Ayala Avenue, 
                                Makati City, 1226 
                                Metro Manila, Philippines
                            </p>
                        </div>
                    </div>
                    <div class="careers__contact__us__container--icon--container">
                        <div class="careers__contact__us__container--icon--image">
                            <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                        </div>
                        <div class="careers__contact__us__container--icon--description">
                            <p>Tel: (632) 889-10307 </p>
                            <p> Fax: (632) 819-0872 / 
                                (632) 818-1377
                            </p>
                        </div>
                    </div>
                    <div class="careers__contact__us__container--icon--container">
                        <div class="careers__contact__us__container--icon--image">
                            <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                        </div>
                        <div class="careers__contact__us__container--icon--description">
                            <p>info@sgv.ph
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
