<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/services-style.css">

<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>Services</h1></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row-divider"></div>

    <div class="services__container">
        <div class="container">
            <div class="row">

                <div class="services__container--icon--row">
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Assurance</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/Material icons Negative-06-86.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Consulting</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/consulting.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Tax</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/tax.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon selected__services">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Strategy & Transactions</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/strat2.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="services__main__container">
                    <div class="services__left">
                        <div>
                            <span class="services__left--title">Maintaining Your Trust
                            </span>
                        </div>
                        <div>
                            <span class="services__left--description">To grow and create value, your company must have the trust of its customers, lenders, and investors. <br/><br/>

The integrity of your company’s financial information and the reliability and security of your technology are critical components in garnering this trust and gaining access to global capital markets. Whether you are managing issues associated with globalization, addressing technology vulnerabilities in the connected economy, or looking to assure the fairness of your financial information — SGV & Co. can help.<br/><br/>

We have invested substantially in innovating and digitalizing our global risk-based methodology and the related technology-based knowledge and enablers. Our auditors are trained on our digital audit methodology, data analytics and documentation tools.<br/><br/>

The methodology is supplemented by comprehensive standards and quality controls that are applicable to every client engagement.
                            </span>
                        </div>
                        <div class="service__left--assurance--image">
                            <img src="./assets/images/service-ass1.png" alt="">
                        </div>
                        <div class="services__left--button service__left--assurance--download--button">
                            <span class="services__left--download__pdf send__message__button">
                                Download PDF
                                <img src="./assets/images/chevron-right1.png" alt="">
                            </span>
                        </div>
                        <div class="services__left--odd--even--content service__left--assurance">
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/auditrev1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                        Audit, Review, and Related Services
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas faucibus placerat. Maecenas neque ex, tempus ac orci ac, pharetra feugiat odio. Duis blandit eget tortor vel tempus. Donec tempus nibh quis interdum.
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                 External audit services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide independent assurance on financial and nonfinancial information to meet regulatory and other stakeholder requirements utilizing world-class business process-based methodologies and supporting tools.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Review services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide review services by performing inquiry and analytical procedures that provide the company with a reasonable basis for expressing limited assurance that there are no material modifications that should be made to the statements in order for them to be in conformity with generally accepted accounting principles or, if applicable, with another comprehensive basis of accounting. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Security offerings
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide assurance services with respect to equity or debt securities offerings by audit clients, including prospectus review, consent to include our report in connection with our audit or review of financial statements, proforma financial information, and issuance of comfort letter. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Agreed-upon procedures
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We perform agreed-upon procedures, in accordance with relevant professional standards, applied to financial or nonfinancial data to which the accountant and the client and any appropriate third parties have agreed, and to report on factual findings. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Extended audit services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide customized assurance services beyond the traditional external audit scope, such as services on branch project audit, audit of specific account, and fixed asset count.
                                </span>
                            </div>
                        </div>
                         <div class="services__assurance--divider row-divider"></div>
                         <div class="services__left--odd--even--content service__left--assurance">
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/finance1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                        Audit, Review, and Related Services
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        We offer a wide range of services that help businesses keep up with evolving market conditions, increase transparency and comply with new regulatory requirements. 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="service__left--assurance--finance--title">
                            <h3>
                                Finance support services
                            </h3>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Corporate treasury and commodities
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies shift toward centralization of the treasury management function. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Consolidation and reporting
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide assistance on every aspect of the consolidation and reporting process, helping businesses to achieve faster, higher quality reporting, control risks, and develop their financial acumen.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Remediation services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Restoring confidence in financial reporting can rebuild market confidence, mitigate the risk of future occurrences and signal to investors, regulators and stakeholders that the company is heading in the right direction. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Corporate governance and regulation
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Governance processes in a company are triggered by globalization, expanding risk factors, developing technologies and society’s expectations. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Training services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide a range of business-specific training services for companies, delivering technical knowledge and a global perspective.
                                </span>
                            </div>
                        </div>
                        <div class="service__left--assurance--finance--title">
                            <h3>
                            Systems, process and analytics
</h3>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Analytics and data intelligence
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                FAAS analytics is the collection, combination and exploration of financial data to help organizations create insights that support their decision-making processes.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Advanced processes
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We support companies in the design and implementation of automated internal control systems, including internal control workflow, which establishes continuous process assurance while increasing efficiency.
                                </span>
                            </div>
                        </div>
                        <div class="service__left--assurance--finance--title">
                            <h3>
                            Accounting and reporting
</h3>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Accounting advice
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide organizations with support when entering into contracts, combining deep commercial and accounting insights, helping management make the most appropriate commercial and accounting decisions.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Accounting change
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help address your complex technical accounting questions.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Government and regulatory accounting
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We cover all aspects of accounting and financial reporting exclusively for the government and public sector, including International Public Sector Accounting Standards (IPSAS), International Financial Reporting Standards (IFRS) and Philippine Financial Reporting Standards (PFRS).
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Management accounting
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies align their internal reporting with their accounting policies.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Accounting policies and procedures
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Accounting policies are the cornerstone of an organization’s financial control environment, bridging the gap between technical accounting literature and business practice.
                                </span>
                            </div>
                        </div>
                        <div class="service__left--assurance--finance--title">
                            <h3>
                            Transaction accounting
</h3>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies enhance value by supporting management as they execute transformational events, such as an acquisition or divestiture, to inspire stakeholder confidence and reflect well on the business, its brand, management and directors. 
                                </span>
                            </div>
                        </div>
                        <div class="service__left--assurance--finance--title">
                            <h3>
                            Managed services
</h3>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Managed services are structured as a strategic long-term arrangement to provide a select set of value-added services addressing the client’s most challenging requirements. 
                                </span>
                            </div>
                        </div>
                        <div class="services__assurance--divider row-divider"></div>
                        <div class="services__left--odd--even--content service__left--assurance">
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/forensic1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                        Forensic & Integrity Services
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        We understand how organizations navigate complex environments; how pressures, attitudes and culture influence employee actions; and how to leverage data analytics to improve compliance and investigation outcomes. We are committed to making integrity the cornerstone of a better working world.
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Investigation and compliance
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Experienced in forensic accounting, technology systems, and risk management, we investigate areas where clients suspect there is a problem.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Transaction forensics
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our Forensic Diligence professionals assist organizations and legal counsel in identifying and mitigating financial, operational and reputational risks inherent to mergers and acquisition transactions.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Claims & disputes
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies and their legal counsel to prepare the calculations, analyses and strategies required for complex commercial disputes.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Forensic data analytics
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We work in engagement teams to carry out both reactive and proactive forensic analysis on large amounts of data that involve data linking, statistics and predictive modeling. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Privacy & cyber response
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We perform computer forensic analysis using the latest technology to assist investigations and discover electronic evidence. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Discovery
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our Discovery, information governance and forensic professionals can help answer the “who, what, where, when and how” questions that help organizations meet their discovery and compliance needs. 
                                </span>
                            </div>
                        </div>
                        <div class="services__assurance--divider row-divider"></div>
                        <div class="services__left--odd--even--content service__left--assurance">
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/climate1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                        Climate Change and Sustainability Services (CCaSS)
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        We help clients identify, manage, and deliver strategies that address nonfinancial matters such as environmental and social risks. We assist our clients in building their resilience to navigate their changing business environment in the long-term.
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Sustainability advisory and supply chain
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help organizations understand their emerging sustainability, supply chain or environment, social and governance (ESG) risks (e.g., human rights, modern slavery, resource constraints, climate change, conflict minerals), build resilience and strengthen their ‘social license to grow’ so that they can survive and grow in an increasingly competitive and accountable operating environment. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Nonfinancial reporting advisory and assurance
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help organizations transition to new ways of communicating their sustainability performance and address the demand of other key stakeholders for greater and more meaningful assurance over nonfinancial disclosures.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Claims & disputes
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies and their legal counsel to prepare the calculations, analyses and strategies required for complex commercial disputes.
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Outcomes measurement
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies understand and evaluate the broader value impacts and outcomes associated with their operations, programs and projects. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Climate change
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                In today’s environment, businesses can no longer ignore the physical and regulatory impacts of climate change on their operations. 
                                </span>
                            </div>
                        </div>
                        <div class="services__left--assurance--container--text">
                            <div class="services__left--assurance--container--text--title">
                                <span class="">
                                Environment, health and safety
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist organizations so that they can make strategic and operational decisions that drive the effective management of environment, health and safety, with an aim to prevent incidents, or in the worst of outcomes – a workplace fatality or environmental catastrophe. 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="services__right">
                        <div class="services__right--row">
                            <div class="header__content">
                                <div class="header__content--sub-title">
                                    <div class="header__divider"></div>
                                    <div class="header__content--home--title">
                                        <span>HOME</span>
                                    </div>
                                </div>
                                <div class="header__content--main--title">
                                    <div class="contact__form__container--main--title"><h3><span>Financial Services</span> Organization</h3></div>
                                    <div class="header__content--description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod elit pretium dignissim efficitur. Vivamus finibus, metus in viverra fermentum, dui mi aliquet risus, vitae finibus metus justo interdum orci. Quisque mollis in quam eget consectetur. Curabitur lobortis varius nibh quis sodales. Nullam porttitor ornare elementum. Aenean vel erat eu turpis suscipit fringilla finibus in libero. Sed tristique, mauris in feugiat congue, magna lectus consectetur ipsum, at laoreet nibh lectus sit amet dolor.
                                    <br /><br />
                                    Vestibulum turpis felis, tempor in consectetur quis, fringilla sit amet ante. Pellentesque ultrices elit ut massa blandit varius. Quisque placerat, libero in dictum facilisis, leo risus maximus turpis</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="contact__form__container--icon">
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                            <p class="contact__form__container--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                    <div class="map__and__form__container">
                        <div class="map__container">
                            <img src="./assets/images/Group 305.png"  alt="" />
                        </div>
                        <div class="contact__form--right">
                            <div>
                                <div class="contact__form--row">
                                    <span>Your name</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Your Email</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Subject</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Message</span>
                                    <textarea name="" id="" cols="30" rows="10" class="input_form"></textarea>
                                </div> 
                                <div class="contact__form--row mg-top">
                                   <input type="button" class="send__message__button" value="Send Message">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</body>