<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/foundation-style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>Home</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>SGV Foundation<br/> <span>For a Better Philippines</span></h1></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="the__latest__foundation__container--right--content--row--menu--list--mobile--view">
        <ul>
        <li class="mobile__view--svgf--home">
           <div>
           SGVF Home
        </div>
           <div class="mobile__view--svgf--img">
               <img src="./assets/images/chev-down.png" alt="">
            </div>
        </li>
            <li>News</li>
            <li>Programs</li>
            <li>Gallery</li>
            <li>Contact</li>
        </ul>
    </div>

    <div class="the__latest__foundation__container">
       <div class="container">
               
           <div class="row">
          
               <div class="the__latest__foundation__container--row">
                    <div class="the__latest__foundation__container--row--left--content">
                        <div class="header__content">
                            <div class="header__content--sub-title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>Home</span>
                                </div>
                            </div>
                            <div class="header__content--main--title--row">
                                <div class="header__content--main--title"><h2><span>The Latest</span><br/> From the Foundation</h2></div>
                                <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                            </div>
                        </div>
                        <div class="the__latest__foundation__container--content">
                            <div class="the__latest__foundation__container--foundation--content">
                                <div class="the__latest__foundation__container--date--time">
                                        <div>
                                            <img src="./assets/images/green-time.png" alt="">
                                        </div>
                                        <div>
                                            <span>June 29, 2020</span>
                                        </div>
                                </div>
                                <h3>DSWD certifies the SGVF as an auxiliary SWDA</h3>
                                <div class="the__latest__foundation__container--office--content--address">
                                    <p>In last week’s article, we discussed the salient features of the CREATE bill: the immediate 
    Corporate Income Tax (CIT) rate cut;
                                    </p>
                                </div>
                                <div class="the__latest__foundation__container--office--content--address">
                                    <span class="the__latest__foundation__container--register--button">
                                        Read more
                                        <img src="./assets/images/chevron-right1.png" alt="">
                                    </span>
                                </div>
                            </div>
                            <div class="the__latest__foundation__container--sub--title-divider header__divider"></div>
                            <div class="the__latest__foundation__container--foundation--content">
                                <div class="the__latest__foundation__container--date--time">
                                        <div>
                                            <img src="./assets/images/green-time.png" alt="">
                                        </div>
                                        <div>
                                            <span>June 29, 2020</span>
                                        </div>
                                </div>
                                <h3>DSWD certifies the SGVF as an auxiliary SWDA</h3>
                                <div class="the__latest__foundation__container--office--content--address">
                                    <p>In last week’s article, we discussed the salient features of the CREATE bill: the immediate 
    Corporate Income Tax (CIT) rate cut;
                                    </p>
                                </div>
                                <div class="the__latest__foundation__container--office--content--address">
                                    <span class="the__latest__foundation__container--register--button">
                                        Read more
                                        <img src="./assets/images/chevron-right1.png" alt="">
                                    </span>
                                </div>
                            </div>
                            <div class="the__latest__foundation__container--sub--title-divider header__divider"></div>
                            <div class="the__latest__foundation__container--foundation--content">
                                <div class="the__latest__foundation__container--date--time">
                                        <div>
                                            <img src="./assets/images/green-time.png" alt="">
                                        </div>
                                        <div>
                                            <span>June 29, 2020</span>
                                        </div>
                                </div>
                                <h3>DSWD certifies the SGVF as an auxiliary SWDA</h3>
                                <div class="the__latest__foundation__container--office--content--address">
                                    <p>In last week’s article, we discussed the salient features of the CREATE bill: the immediate 
    Corporate Income Tax (CIT) rate cut;
                                    </p>
                                </div>
                                <div class="the__latest__foundation__container--office--content--address">
                                    <span class="the__latest__foundation__container--register--button">
                                        Read more
                                        <img src="./assets/images/chevron-right1.png" alt="">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="the__latest__foundation__container--view--more--button">
                            <span class="create__connection__container--register--button view__more__button ">
                                    View more
                                <img src="./assets/images/chevron-white.png" alt="">
                            </span>
                        </div>
                    </div>
                    <div class="the__latest__foundation__container--right--content">
                        <div class="the__latest__foundation__container--right--content--row">
                            <div class="the__latest__foundation__container--right--content--row-inner">
                                <div class="the__latest__foundation__container--right--content--row--image">
                                    <img src="./assets/images/sgvf-logo.png" alt="">
                                </div>
                                <div class="the__latest__foundation__container--right--content--row--menu--list">
                                    <div class="the__latest__foundation__container--right--content--row--menu--list--title">
                                        <div class="the__latest__foundation__container--right--content--row--menu--list--divider"></div>
                                        <div >
                                            <p>
                                            SGVF Home
                                            </p>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>News</li>
                                        <li>Programs</li>
                                        <li>Gallery</li>
                                        <li>Contact</li>
                                    </ul>
                                    <div class="the__latest__foundation__container--right--content--row--divider"></div>
                                    <div class="the__latest__foundation__container--right--content--row--reg--no">
                                        <p>PCNC Registration No. 0852010</p>
                                    </div>
                                    <div class="the__latest__foundation__container--right--content--row--icon--with--text">
                                        <div>
                                            <img src="./assets/images/green-map-pin.png" alt="">
                                        </div>
                                        <div>
                                            <p class="the__latest__foundation__container--right--content--row--icon--with--text--title">
                                                Makati Head Office
                                            </p>
                                            <p>
                                            SGV-1, 6760 Ayala Ave., <br />Makati City Philippines
                                            </p>
                                        </div>
                                    </div>
                                    <div class="the__latest__foundation__container--right--content--row--icon--with--text the__latest__foundation__container--right--content--row--call--us">
                                        <div>
                                            <img src="./assets/images/green-phone.png" alt="">
                                        </div>
                                        <div>
                                            <p class="the__latest__foundation__container--right--content--row--icon--with--text--title">
                                            Call Us
                                            </p>
                                            <p>
                                            (632)8910307 loc. 7799
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
           </div>
       </div>
    </div>

    <div class="about__svgf__container">
        <div class="container">
            <div class="row">
                <div class="about__svgf__row">
                    <div class="about__svgf__row--left--content">
                        <div class="header__content">
                            <div class="header__content--sub-title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>About Us</span>
                                </div>
                            </div>
                            <div class="header__content--main--title">
                                <div class="contact__form__container--main--title"><h2>About <span>SVG </span> </h2></div>
                            </div>
                        </div>
                    </div>
                    <div class="about__svgf__row--row--right">
                        <img src="./assets/images/quote-green.png" alt="">
                        <p class="about__svgf__row--row--right--title">The insights and quality services we provide help 
build trust and confidence in the capital markets and 
in economies the world over. In so doing, we play a 
critical role in building a better working world for our 
people, for our clients and for our communities.</p>
                    </div>
                </div>

                <div class="about--divider"></div>

                <div class="about__svgf__three__columns__row">
                    <div>
                        <span>
                        In 1966, on SGV’s 20th year, Washington SyCip, together with co-founder Alfredo M. Velayo and partners Cesar E. A. Virata, Benjamin V. Abela, and Erlinda T. Villanueva, established the SGV Foundation, Inc.
                        <br/> <br/>
W. SyCip relates in his biography that he wanted to develop social consciousness among his partners, to show their concern for the country and the community. With their good income, he made them contribute to the SGV Foundation. He set the example himself by contributing larger than anyone else. Since then, it has become a welcome practice for SGV and its partners to contribute part of their annual earnings to the Foundation. 
                        </span>
                    </div>
                    <div>
                        <span>
                        SGV and its people, too, have made the Foundation the channel of their social responsibility undertakings.<br/> <br/>

In adherence to its primary purposes, the Foundation for the last 45 years has encouraged the promotion of the industrial, business and management sciences through financial assistance and support, grants, scholarships, and research. Learning institutions, entrepreneurs, researchers, students and educators have been main beneficiaries of these efforts.
<br/> <br/>
Alongside, the Foundation has undertaken projects relating to social, moral and economic development of 
                        </span>
                    </div>
                    <div>
                        <span>
                        communities and families with the end in view of promoting the general welfare of society. <br/> <br/>

Today, the Foundation also partners with other foundations in various projects that are aligned with its goals and objectives. <br/> <br/>

The Foundation is accredited with the Philippine Council for NGO Certification (PCNC) as a qualified donee-institution recognized by the Bureau of Internal Revenue. It is a member of the League of Corporate Foundations and Association of Foundations.
                        </span>
                    </div>
                </div>
                <div class="about__svgf__members">
                    <img src="./assets/images/svgf-members.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="our__foundation__mission__container">
        <div class="container">
            <div class="row">
                <div class="our__foundation__mission__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Our Foundation Mission
</span> <br>and Vision</h2></div>
                        </div>
                    </div>
                    <div class="our__mission__vision__row">
                        <div class="our__mission__vision__row--mission">
                        <h3>
                         The Mission
</h3>
                        <p>
                        To spearhead social, moral and economic programs that will inculcate the
values of education, discipline, integrity and excellence in Filipinos.
                        </p>
                        </div>
                        <div  class="our__mission__vision__row--vission">
                        <h3>
                        The Vision
</h3>
                        <p>
                        To leverage knowledge, learning and other social development initiatives as
catalysts for improving the quality of life of Filipino families and communities.
                        </p>
                        </div>
                    </div>
                    <div class="our__foundation__mission__container--button--row">
                        <span class="our__foundation__mission__button">
                            Board of Trustees
                            <img src="./assets/images/chevron-white.png" alt="">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="svgf__programs__container">
        <div class="container">
            <div class="row">
                <div class="svgf__programs__row">
                    <div class="svgf__programs__row--title-button">
                        <div class="header__content">
                            <div class="header__content--sub-title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>INVESTING IN PEOPLE</span>
                                </div>
                            </div>
                            <div class="header__content--main--title">
                                <div class="contact__form__container--main--title"><h2>SGVF  <span>Programs</span></h2></div>
                            </div>
                        </div>
                        <div class="our__foundation__mission__container--button--row">
                            <span class="our__foundation__mission__button">
                                View all programs
                                <img src="./assets/images/chevron-white.png" alt="">
                            </span>
                        </div>
                    </div>
                    <div class="svgf__programs__row--icon--row">
                        <div class="svgf__programs__row--icon">
                            <div class="svgf__programs__row--icon--list">
                                <div class="svgf__programs__row--icon--title">
                                    <span>Education</span>
                                </div>
                                <div class="svgf__programs__row--icon--image">
                                    <img src="./assets/images/educ1.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="svgf__programs__row--icon">
                            <div class="svgf__programs__row--icon--list">
                                <div class="svgf__programs__row--icon--title">
                                    <span>Entrepreneurship</span>
                                </div>
                                <div class="svgf__programs__row--icon--image">
                                    <img src="./assets/images/entrep1.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="svgf__programs__row--icon">
                            <div class="svgf__programs__row--icon--list">
                                <div class="svgf__programs__row--icon--title">
                                    <span>Social and Community
Development</span>
                                </div>
                                <div class="svgf__programs__row--icon--image">
                                    <img src="./assets/images/social1.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="the__foundation__action__row">
                        <div class="header__content">
                            <div class="header__content--sub-title">
                                <div class="header__divider"></div>
                                <div class="header__content--home--title">
                                    <span>INVESTING IN PEOPLE</span>
                                </div>
                            </div>
                            <div class="header__content--main--title">
                                <div class="contact__form__container--main--title"><h2>The Foundation <br><span>In Action</span></h2></div>
                            </div>
                        </div>
                    
                    
                    <div class="the__foundation__acction__masonry__row">
                        <div class="the__foundation__acction__masonry__row--container">
                            <div class="the__foundation__acction__masonry__row--container--hover">
                                <div class="the__foundation__acction__masonry__row--container-row "
                                style="background-image:url(./assets/images/action6.jpg)"
                                >
                                    <div class="the__foundation__acction__masonry__row--container--overlay">
                                    <p>Board Meetings</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="the__foundation__acction__masonry__row--container">
                            <div class="the__foundation__acction__masonry__row--container--hover">
                                <div class="the__foundation__acction__masonry__row--container-row "
                                style="background-image:url(./assets/images/action5.jpg)"
                                >
                                    <div class="the__foundation__acction__masonry__row--container--overlay">
                                    <p>Book Sponsorships</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="the__foundation__acction__masonry__row--container">
                            <div class="the__foundation__acction__masonry__row--container--hover">
                                <div class="the__foundation__acction__masonry__row--container-row "
                                style="background-image:url(./assets/images/action4.jpg)"
                                >
                                    <div class="the__foundation__acction__masonry__row--container--overlay">
                                    <p>Scholarships</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="the__foundation__acction__masonry__row--container">
                            <div class="the__foundation__acction__masonry__row--container--hover">
                                <div class="the__foundation__acction__masonry__row--container-row "
                                style="background-image:url(./assets/images/action3.jpg)"
                                >
                                    <div class="the__foundation__acction__masonry__row--container--overlay">
                                    <p>Community Development</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="the__foundation__acction__masonry__row--container">
                            <div class="the__foundation__acction__masonry__row--container--hover">
                                <div class="the__foundation__acction__masonry__row--container-row "
                                style="background-image:url(./assets/images/action2.jpg)"
                                >
                                    <div class="the__foundation__acction__masonry__row--container--overlay">
                                    <p>Donations & Grants</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="the__foundation__acction__masonry__row--container">
                            <div class="the__foundation__acction__masonry__row--container--hover">
                                <div class="the__foundation__acction__masonry__row--container-row "
                                style="background-image:url(./assets/images/action1.jpg)"
                                >
                                    <div class="the__foundation__acction__masonry__row--container--overlay">
                                    <p>Entrepreneurship</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="foundation__get__in__touch--icon">
                        <div class="foundation__get__in__touch--icon--container">
                            <div class="foundation__get__in__touch--icon--image">
                                <img src="./assets/images/location-white.png" alt="">
                            </div>
                            <div class="foundation__get__in__touch--icon--description">
                                <p class="foundation__get__in__touch--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="foundation__get__in__touch--icon--container">
                            <div class="foundation__get__in__touch--icon--image">
                                <img src="./assets/images/phone-white.png" alt="">
                            </div>
                            <div class="foundation__get__in__touch--icon--description">
                            <p class="foundation__get__in__touch--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="foundation__get__in__touch--icon--container">
                            <div class="foundation__get__in__touch--icon--image">
                                <img src="./assets/images/email-white.png" alt="">
                            </div>
                            <div class="foundation__get__in__touch--icon--description">
                                <p class="foundation__get__in__touch--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="map__and__form__container">
                    <div class="map__container">
                        <img src="./assets/images/green-map.png" alt="">
                    </div>
                    <div class="contact__form--right">
                        <div>
                            <div class="contact__form--row">
                                <span>Your name</span>
                                <input type="text" class="input_form">
                            </div>
                            <div class="contact__form--row mg-top">
                                <span>Your Email</span>
                                <input type="text" class="input_form">
                            </div>
                            <div class="contact__form--row mg-top">
                                <span>Subject</span>
                                <input type="text" class="input_form">
                            </div>
                            <div class="contact__form--row mg-top">
                                <span>Message</span>
                                <textarea name="" id="" cols="30" rows="10" class="input_form"></textarea>
                            </div> 
                            <div class="contact__form--row mg-top">
                                <input type="button" class="send__message__button" value="Send Message">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
