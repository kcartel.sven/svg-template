<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/alumni-registration-style.css">
<link rel="stylesheet" href="./assets/css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>EVENTS & SEMINARS</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>PFRS Changes in 2019 and Beyond PRF 16, Leases Workshop</div>
                            <!-- <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row-divider"></div>

    <div class="alumni__registration__container">
        <div class="container">
            <div class="row">
                <div class="alumni__registration__container--register--title">
                    <h2>
                        Register Now!
                    </h2>
                </div>
                <div class="alumni__registration__container__row">
                    <div class="alumni__registration__container__row--left">
                        <h3>About You</h3>
                        <!-- <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>EVENTS &amp; SEMINARS</span>
                            </div>
                        </div> -->
                        <div class="alumni__registration__container__row--left--forms">
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Firstname
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Middlename
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Lastname
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Nickname
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Home Address
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Country
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <select name="" id="" class="input_form">
                                        <option value="">Select...</option>
                                    </select>
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Birthdate
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="date" class="input_form">
                                </span>
                                <!-- <span class="alumni_-registration__container__row--birthdate">
                                    <span>
                                        Month
                                        <select name="" id="" class="input_form">
                                            <option value="">Select...</option>
                                        </select>
                                    </span>
                                    <span >
                                        Day
                                        <select name="" id="" class="input_form">
                                            <option value="">Select...</option>
                                        </select>
                                    </span>
                                    <span >
                                        Year
                                        <select name="" id="" class="input_form">
                                            <option value="">Select...</option>
                                        </select>
                                    </span>
                                </span> -->
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                Home Telephone Number
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                Mobile Number
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="alumni__registration__container__row--input--row">
                                <span class="alumni__registration__container__row--input--title">
                                    Email
                                </span>
                                <span class="alumni__registration__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                        </div>
                        <div class="row-divider"></div>
                        <div>
                            <h3>Your Current Work</h3>
                            <div class="alumni__registration__container__row--left--forms">
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Company 
Name
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Corporate Email
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Current Position
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Business Address
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Business Number
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Business Fax
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="row-divider"></div>
                        <div>
                            <h3>When You Were In SGV</h3>
                            <div class="alumni__registration__container__row--left--forms">
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Market Circle
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Last Partner
Incharge
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Year Joined SGV
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                    <select name="" id="" class="input_form">
                                            <option value="">Select...</option>
                                        </select>
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Year Resigned/
Retired
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                    <select name="" id="" class="input_form">
                                            <option value="">Select...</option>
                                        </select>
                                    </span>
                                </span>
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Business Fax
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="row-divider"></div>
                        <div>
                            <h3>Just To Confirm</h3>
                            <div class="alumni__registration__container__row--left--forms">
                                <span class="alumni__registration__container__row--input--row">
                                    <span class="alumni__registration__container__row--input--title">
                                    Are You Human?
                                    </span>
                                    <span class="alumni__registration__container__row--input">
                                        <input type="text" class="input_form" placeholder="Solve for x: ( x + 2 ) 2 − 4 + ( y − 1 ) 2 − 1 = x ">
                                    </span>
                                </span>
                                <div class="alumni__registration__container__row--terms--title">
                                    <p>
                                    Do You Accept Our Terms & Conditions
                                    </p>
                                </div>
                                <div class="alumni__registration__container__row--terms--checkbox">
                                    <label class="alumni__registration__container__row--input--checkbox">
                                        <input type="checkbox" >
                                    <span class="checkmark"></span>
                                    Yes, I accept <br/>
                                    Please see our full Terms and Conditions.
                                    </label>
                                    
                                </div>                                    
                            </div>
                        </div>
                        <div class="alumni__registration__container__row--button--row">
                            <input type="button" class="send__message__button" value="Ready To Join"/>
                        </div>
                    </div>
                    <div class="alumni__registration__container__row--right">
                        <h2>
                            You May Contact Us
                       </h2>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Makati – Head Office
                            </h3>
                            <p>
                            6760 Ayala Avenue, Makati City, <br/>
                            1226 Metro Manila, Philippines<br/>
                            Tel: (632) 889-10307<br/>
                            Fax: (632) 819-0872 / (632) 818-1377
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Bacolod
                            </h3>
                            <p>
                            No. 5 & 17 2nd floor MC Metroplex B.S.<br/>
                            Aquino Drive, Villamonte, Bacolod City,<br/>
                            6100, Negros Occidental, Philippines<br/>
                            Tel: (034) 441-2861 to 62<br/>
                            Fax: (034) 441-2863
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Baguio
                            </h3>
                            <p>
                            3F Insular Life Bldg.,<br/>
                            Legarda Road corner Abanao St.,<br/>
                            Baguio City 2600<br/>
                            Tel: (074) 443-9858<br/>
                            Fax: (074) 442-6509
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Cagayan de Oro
                            </h3>
                            <p>
                            Suites 4 & 5, Fourth Level <br/>
                            Gateway Tower 1, Limketkai Center<br/>
                            Lapasan, Cagayan de Oro City<br/>
                            Tel: (08822) 726-555,<br/>
                            (08822) 725-078<br/>
                            Fax: (088) 856-4415
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Cavite
                            </h3>
                            <p>
                            Metrobank Rosario Branch Gen Trias Drive, <br/>
                            Tejero Rosario, Cavite<br/>
                            Tel: (046) 437 7780<br/>
                            (02) 845 2065<br/>
                            Fax: (02) 741 1375<br/>
                            (046) 437 8059<br/>
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Cebu
                            </h3>
                            <p>
                            Unit 1003 & 1004, Insular Life Cebu Business Centre <br/>
                            Mindanao Avenue corner Biliran Road<br/>
                            Cebu Business Park, Cebu City<br/>
                            Tel.: (032) 266-2947 to 49,<br/>
                            (032) 266-2821<br/>
                            (032) 266-2951<br/>
                            Fax: (032) 266-2313
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Clark
                            </h3>
                            <p>
                            2nd Floor, 2 WorkPlus Building <br/>
                            Mimosa Drive, Filinvest Mimosa Leisure City <br/>
                            Clark Freeport Zone, Philippines, 2023 <br/>
                            Mobile: (+63) 917-5656604 <br/>
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            Davao
                            </h3>
                            <p>
                            5th floor Topaz Tower, Damosa IT Park <br/>
                            J. P Laurel Avenue, Lanang <br/>
                            8000 Davao City, Philippines <br/>
                            Tel: (082) 282-8447 to 51 <br/>
                            Fax: (082) 282-8438 <br/>
                            </p>
                        </div>
                        <div    class="alumni__registration__container__row--right--content--seminar">
                            <h3>
                            General Santos
                            </h3>
                            <p>
                            2F Elan 3 Building <br/>
                            Roxas Avenue, Dadiangas East <br/>
                            General Santos City <br/>
                            9500 South Cotabato<br/> 
                            Tel: (083) 552-7001 to  02<br/>
                            Fax: (083) 552-5314
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="create__connection__container">
        <div class="create__connection__container--row--left">
            <div class="create__connection__container--row--left--gradient">
                <div class="header__content">
                    <div class="header__content--sub-title">
                        <div class="header__divider"></div>
                        <div class="header__content--home--title">
                            <span>WE VALUE TRUST & INTEGRITY IN EVERYTHING WE DO</span>
                        </div>
                    </div>
                    <div class="header__content--main--title">
                        <div class="contact__form__container--main--title"><h2>Create Connections For Life </h2>
                        </div>
                    </div>
                    <div class="header__content--description">
                        <p>
                        Wherever your working world takes you, your SGV family is always delighted 
    to welcome you home.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="create__connection__container--row--right">
            <div class="create__connection__container--button--row">
                <input type="button" class="send__message__button" value="View all SGV alumni news"/>
            </div>
            <div class="header__content">
                <div class="header__content--sub-title">
                    <div class="header__divider"></div>
                    <div class="header__content--home--title">
                        <span>HOME</span>
                    </div>
                </div>
                <div class="header__content--main--title">
                    <div class="contact__form__container--main--title"><h2><span>SGV Alumni</span><br/> News and Events</h2></div>
                </div>
            </div>
            <div class="create__connection__container--office--row">
                <div class="create__connection__container--office--content">
                    <h3>Risk Management Seminar</h3>
                    <div class="create__connection__container--office--content--address">
                        <p>As an accredited institutional training 
                            provider on Corporate Governance, SGV
                            will be holding a seminar that will discuss
                            Corporate Governance principles and
                            leading practices.
                        </p>
                    </div>
                    <div class="create__connection__container--office--content--address">
                        <div class="create__connection__container--sub-title">
                            <div class="create__connection__container--sub--title-divider header__divider"></div>
                            <div class="create__connection__container--home--title">
                                <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                            </div>
                        </div>
                    </div>
                    <div class="create__connection__container--office--content--address">
                        <span class="create__connection__container--register--button">
                            Register Now
                            <img src="./assets/images/chevron-right1.png" alt="">
                        </span>
                    </div>
                </div>
                <div class="create__connection__container--office--content">
                    <h3>Corporate Governance Seminar</h3>
                    <div class="create__connection__container--office--content--address">
                        <p>As an accredited institutional training 
                            provider on Corporate Governance, SGV
                            will be holding a seminar that will discuss
                            Corporate Governance principles and
                            leading practices.
                        </p>
                    </div>
                    <div class="create__connection__container--office--content--address">
                        <div class="create__connection__container--sub-title">
                            <div class="create__connection__container--sub--title-divider header__divider"></div>
                            <div class="create__connection__container--home--title">
                                <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                            </div>
                        </div>
                    </div>
                    <div class="create__connection__container--office--content--address">
                        <span class="create__connection__container--register--button">
                            Register now
                            <img src="./assets/images/chevron-right1.png" alt="">
                        </span>
                    </div>
                </div>
            </div>
            <div class="create__connection__container--paginate--row">
                <div class="create__connection__container--paginate--not--selected"></div>
                <div class="create__connection__container--paginate--selected"></div>
                <div class="create__connection__container--paginate--not--selected"></div>
                <div class="create__connection__container--paginate--not--selected"></div>
            </div>
        </div>
    </div>

</body>
</html>
