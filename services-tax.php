<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/services-style.css">

<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>Services</h1></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row-divider"></div>

    <div class="services__container">
        <div class="container">
            <div class="row">

            <div class="services__container--icon--row">
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Assurance</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/Material icons Negative-06-86.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Consulting</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/consulting.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Tax</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/tax.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon selected__services">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Strategy <br/>& Transactions</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/strat2.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="services__main__container">
                    <div class="services__left">
                        <div>
                            <span class="services__left--title">Driven to Exceed Your Expectations
                            </span>
                        </div>
                        <div>
                            <span class="services__left--description">As today’s business environment becomes increasingly complex with a host of tax rules and regulations, it is imperative that businesses equip themselves with the best advice on these matters to compete effectively and efficiently. <br/><br/>

NOW, more than ever, enterprises need to find new ways to align their tax strategies with their overall business needs, while meeting their compliance obligations wherever they arise. Our Tax Services can help you make these things happen by assisting you in identifying and implementing the best possible approach to all tax and business concerns.
                            </span>
                        </div>
                        <div class="service__left--assurance--image">
                            <img src="./assets/images/serv-tax1.png" alt="">
                        </div>
                        <div class="services__left--button service__left--assurance--download--button">
                            <span class="services__left--download__pdf send__message__button">
                                Download PDF
                                <img src="./assets/images/chevron-right1.png" alt="">
                            </span>
                        </div>
                        <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Business Tax Compliance
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our tax professionals can provide assistance in determining a company’s tax liabilities and preparing/reviewing the related returns and attachments (e.g., BIR Form No. 1709-Information Return on Related Party Transactions). 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Health Check
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our tax professionals provide a diagnostic evaluation of a company’s tax position, practices, and procedures to determine whether they comply with Philippine tax laws and regulations.
                                </span>
                            </div>
                        </div>
                        <div class="service__left--tax--divider row-divider"></div>
                        <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Payroll Accounting and Operate (POS)
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our services provide end-to-end payroll delivery using our technology platforms. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Accounting Compliance and Reporting (ACR)
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist companies in various aspects of their accounting operations with our ACR services.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Finance and Operate (TFO)
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Tax Finance and Operate is a technology-driven tax service delivery model that helps organizations reimagine their tax and finance functions resulting in a sustainable corporate tax function that meets each company’s specific needs. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                International Corporate Tax Advisory
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide globally consistent and cost-effective corporate income tax advice on all aspects of cross-border transactions. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Private Client Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We equip clients to make better business decisions by bringing insights to the forefront. We recognize the unique needs of private businesses and their owners.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Estate/ Family Wealth Planning
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We offer advice on how to effectively reduce gift and estate taxes. We provide tax planning for wealth transfer, succession planning for family-owned businesses, tax advice on creation of trusts, and preparation of estate and donor’s tax returns.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Planning Engagement
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We help companies identify opportunities or areas where they can effectively generate tax savings within the parameters allowed by law.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Due Diligence Review
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide Sell Side and Buy Side Tax Due Diligence Services.
                                </span>
                            </div>
                        </div>
                        <div class="service__left--tax--divider row-divider"></div>
                        <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Refinancing Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide advice related to the extraction of cash from corporate or private equity investments or through debt or equity exchanges, the buyback of debt or equity instruments, and the raising of additional debt or equity financing.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Non-Bankruptcy Restructuring Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide advice on legal entity rationalization or other tax effective organizational structure alternatives to help distressed debtors improve their cash flows, reduce operational costs and/or better manage risk where appropriate.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Real Estate Investment Trust (REIT) Advisory
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide advice to potential REIT Sponsors embarking on a conversion to a REIT regime, including the tax implications of transfers of assets to the REIT, the initial public offering and secondary offering of REIT securities, and the reinvestment plans of the REIT Sponsors. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Center for Tax Policy
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                The Center for Tax Policy is a one-stop resource for tax reform, policy and legislation.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Advocacy/ Controversy Work
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist clients in liaising with the tax authorities for requests for rulings or opinions from the Bureau of Internal Revenue and the Department of Finance and in handling controversies and disputes arising from tax assessments. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Independent Certified Public Accountant (CPA) Verification Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist taxpayers who have claims for refund or who have tax assessments by providing a detailed review of the documents supporting their claims for refund or position against the assessments and presenting these to the Court of Tax Appeals.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Assessment Assistance
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist taxpayers in the preparation of the tax schedules/reconciliations in connection with the examination of the taxpayer’s books of accounts and other accounting records related to all internal revenue tax liabilities in a specific period.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Investment Advisory Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our tax experts help prospective investors determine the most cost-and tax-effective vehicle/structure for Philippine inbound and outbound investments.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Individual Tax and Immigration Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our tax and immigration people work together to provide highly-integrated mobility services to address and support mobility needs locally and globally.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                HR Regulatory Compliance
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our team can check and review client’s compliance with the regulatory requirements of the Philippine Social Security System (SSS), Philippine Health Insurance Corporation (PhilHealth), Home Development Mutual Fund (HDMF) and Department of Labor and Employment (DOLE) to determine completeness and timeliness of types of reports submitted to these agencies as an employing company. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                General Advisory Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We offer sound and expert advice on a wide range of tax matters such as those relating to corporate organizations and reorganizations, build-operate-transfer projects, joint venture agreements; various contracts and financial products; revenue legislation and rules, etc.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Transfer Pricing Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We provide organizations with a professional assessment of whether their existing intercompany pricing policies comply with tax laws and regulations by reviewing existing transfer pricing methods and recommending the adoption of the optimal model.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Customs Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our Global Trade & Customs Team assists clients in various matters related to the dealings with the Bureau of Customs (BOC).
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Inderect Tax Compliance
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist our clients in determining or reviewing their VAT (indirect tax) liabilities and/or payments, and in preparing/ reviewing the related returns. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Seminars
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We organize seminars to equip your people with a “Back to Basics” understanding of the Philippine tax system.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Investment Advisory Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our tax experts help prospective investors determine the most cost-and tax-effective vehicle/structure for Philippine inbound and outbound investments.
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Tax Performance Advisory Services/ Tax Technology and Transformation
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                We assist our clients with the strategic and operational challenges facing their tax functions. 
                                </span>
                            </div>
                        </div>
                         <div class="service__left--tax--divider row-divider"></div>
                         <div class="services__left--tax--container--text">
                            <div class="services__left--tax--container--text--title">
                                <span class="">
                                Entity Compliance and Governance Services
                                </span>
                            </div>
                            <div class="services__left--assurance--container--text--description">
                                <span>
                                Our team of experienced lawyers can assist in compliance with SEC reportorial requirements pursuant to the Revised Corporation Code and related issuances. 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="services__right">
                        <div class="services__right--row">
                            <div class="header__content">
                                <div class="header__content--sub-title">
                                    <div class="header__divider"></div>
                                    <div class="header__content--home--title">
                                        <span>HOME</span>
                                    </div>
                                </div>
                                <div class="header__content--main--title">
                                    <div class="contact__form__container--main--title"><h3><span>Financial Services</span> Organization</h3></div>
                                    <div class="header__content--description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod elit pretium dignissim efficitur. Vivamus finibus, metus in viverra fermentum, dui mi aliquet risus, vitae finibus metus justo interdum orci. Quisque mollis in quam eget consectetur. Curabitur lobortis varius nibh quis sodales. Nullam porttitor ornare elementum. Aenean vel erat eu turpis suscipit fringilla finibus in libero. Sed tristique, mauris in feugiat congue, magna lectus consectetur ipsum, at laoreet nibh lectus sit amet dolor.
                                    <br /><br />
                                    Vestibulum turpis felis, tempor in consectetur quis, fringilla sit amet ante. Pellentesque ultrices elit ut massa blandit varius. Quisque placerat, libero in dictum facilisis, leo risus maximus turpis</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="contact__form__container--icon">
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                            <p class="contact__form__container--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                    <div class="map__and__form__container">
                        <div class="map__container">
                            <img src="./assets/images/Group 305.png"  alt="" />
                        </div>
                        <div class="contact__form--right">
                            <div>
                                <div class="contact__form--row">
                                    <span>Your name</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Your Email</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Subject</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Message</span>
                                    <textarea name="" id="" cols="30" rows="10" class="input_form"></textarea>
                                </div> 
                                <div class="contact__form--row mg-top">
                                   <input type="button" class="send__message__button" value="Send Message">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</body>