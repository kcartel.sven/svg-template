<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/services-style.css">

<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>Services</h1></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row-divider"></div>

    <div class="services__container">
        <div class="container">
            <div class="row">

            <div class="services__container--icon--row">
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Assurance</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/Material icons Negative-06-86.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Consulting</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/consulting.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Tax</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/tax.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="services__container--icon selected__services">
                        <div class="services__container--icon--list">
                            <div class="services__container--icon--title">
                                <span>Strategy <br/>& Transactions</span>
                            </div>
                            <div class="services__container--icon--image">
                                <img src="./assets/images/strat2.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="services__main__container">
                    <div class="services__left">
                        <div>
                            <span class="services__left--title">How organizations manage their capital today will define their competitive position tomorrow.
                            </span>
                        </div>
                        <div><span class="services__left--description">Our Strategy and Transactions Services (SaT) team will help guide you in creating financial and economic value for your deals, achieving the best capital performance, and meeting your strategic corporate objectives. Our SaT professionals advise strategies to raise, invest, optimize and preserve capital. We work with you to evaluate opportunities, make your transactions more efficient, and enhance and maximize enterprise value. Whatever the size, nature or location of your company – and your deals – we can help you in every step of the deal life cycle.</div>
                        </span>
                        <div class="services__left--diagram--row">
                            <img src="./assets/images/diagram1.png" alt="">
                        </div>
                        <div class="services__left--text--list">
                            <ul>
                                <li>Raise: How can we win the competition for scarce capital?</li>
                                <li>Invest: How can we seize growth opportunities and competitive advantages?</li>
                                <li>Optimize: How can we increase and maintain investor confidence?</li>
                                <li>Preserve: How can we better anticipate and adapt to market conditions as they change?</li>
                            </ul>
                        </div>
                        <div class="services__left--button">
                            <span class="services__left--download__pdf send__message__button">
                                Download PDF
                                <img src="./assets/images/chevron-right1.png" alt="">
                            </span>
                        </div>
                        <div class="services__left--odd--even--content">
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/strat-real1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                            Strategy Realized
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                            Enabling fast-track growth and portfolio strategies that help you realize your full potential for a better future.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/corp1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                            Corporate Finance
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        Supporting better decisions around financing and funding capital expansion and optimizing capital efficiency.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/buy1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                            Buy and Integrate
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        Enabling fast-track growth and portfolio strategies that help you realize your full potential for a better future.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/sell1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                            Sell and Separate
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                        Enabling strategic portfolio management and better divestments that help you improve value from a sale of an entire company, carve-out, spin off or joint venture.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="services__left--odd--even--row">
                                <div class="services__left--odd--even--image">
                                    <img src="./assets/images/buyint1.png" alt="">
                                </div>
                                <div class="services__left--odd--even--description--row">
                                    <div class="services__left--odd--even--title">
                                        <span>
                                            Buy and Integrate
                                        </span>
                                    </div>
                                    <div class="services__left--odd--even--desc"> 
                                        <span>
                                            Providing trusted leadership in urgent, critical and complex situations to rapidly solve business challenges, sustainably improve results and help you reshape for a better future.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="services__right">
                        <div class="services__right--row">
                            <div class="header__content">
                                <div class="header__content--sub-title">
                                    <div class="header__divider"></div>
                                    <div class="header__content--home--title">
                                        <span>HOME</span>
                                    </div>
                                </div>
                                <div class="header__content--main--title">
                                    <div class="contact__form__container--main--title"><h3><span>Financial Services</span> Organization</h3></div>
                                    <div class="header__content--description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod elit pretium dignissim efficitur. Vivamus finibus, metus in viverra fermentum, dui mi aliquet risus, vitae finibus metus justo interdum orci. Quisque mollis in quam eget consectetur. Curabitur lobortis varius nibh quis sodales. Nullam porttitor ornare elementum. Aenean vel erat eu turpis suscipit fringilla finibus in libero. Sed tristique, mauris in feugiat congue, magna lectus consectetur ipsum, at laoreet nibh lectus sit amet dolor.
                                    <br /><br />
                                    Vestibulum turpis felis, tempor in consectetur quis, fringilla sit amet ante. Pellentesque ultrices elit ut massa blandit varius. Quisque placerat, libero in dictum facilisis, leo risus maximus turpis</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>    
    </div>

    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="contact__form__container--icon">
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                            <p class="contact__form__container--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="contact__form__container--icon--container">
                            <div class="contact__form__container--icon--image">
                                <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                            </div>
                            <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                    <div class="map__and__form__container">
                        <div class="map__container">
                            <img src="./assets/images/Group 305.png"  alt="" />
                        </div>
                        <div class="contact__form--right">
                            <div>
                                <div class="contact__form--row">
                                    <span>Your name</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Your Email</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Subject</span>
                                    <input type="text" class="input_form">
                                </div>
                                <div class="contact__form--row mg-top">
                                    <span>Message</span>
                                    <textarea name="" id="" cols="30" rows="10" class="input_form"></textarea>
                                </div> 
                                <div class="contact__form--row mg-top">
                                   <input type="button" class="send__message__button" value="Send Message">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




</body>
</html>