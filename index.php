<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>Contact Us</h1></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <div class="contact__us__container">
        <div class="container">
            <div class="row">
                <div class="contact__form__container">
                    <div class="contact__form--left">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>REACH OUT TO US</span>
                            </div>
                        </div>
                        <div class="contact__form__container--main--title--row">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="contact__form__container--description"><span>As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</span></div>
                        </div>
                        <div class="divider"></div>
                        <div class="contact__form__container--icon">
                            <div class="contact__form__container--icon--container">
                                <div class="contact__form__container--icon--image">
                                    <img src="./assets/images/Material icons Negative-05-81.png" alt=""/>
                                </div>
                                <div class="contact__form__container--icon--description">
                                    <p class="contact__form__container--icon--title">Address</p>
                                    <p>6760 Ayala Avenue, <br />
                                        Makati City, 1226 <br/>
                                        Metro Manila, Philippines
                                    </p>
                                </div>
                            </div>
                            <div class="contact__form__container--icon--container">
                                <div class="contact__form__container--icon--image">
                                    <img src="./assets/images/Material icons Negative-05-11.png" alt=""/>
                                </div>
                                <div class="contact__form__container--icon--description">
                                <p class="contact__form__container--icon--title">Phone</p>
                                    <p>Tel: (632) 889-10307 <br />
                                        Fax: (632) 819-0872 / <br />
                                        (632) 818-1377
                                    </p>
                                </div>
                            </div>
                            <div class="contact__form__container--icon--container">
                                <div class="contact__form__container--icon--image">
                                    <img src="./assets/images/Material icons Negative-04-51.png" alt=""/>
                                </div>
                                <div class="contact__form__container--icon--description">
                                    <p class="contact__form__container--icon--title">Email</p>
                                    <p>info@sgv.ph
                                    </p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="contact__form--right">
                        <div>
                            <div class="contact__form--row">
                                <span>Your name</span>
                                <input type="text" class="input_form">
                            </div>
                            <div class="contact__form--row mg-top">
                                <span>Your Email</span>
                                <input type="text" class="input_form">
                            </div>
                            <div class="contact__form--row mg-top">
                                <span>Subject</span>
                                <input type="text" class="input_form">
                            </div>
                            <div class="contact__form--row mg-top">
                                <span>Message</span>
                                <textarea name="" id="" cols="30" rows="10" class="input_form"></textarea>
                            </div> 
                            <div class="contact__form--row mg-top">
                                <input type="button" class="send__message__button" value="Send Message"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-divider"></div>

    <div class="office__location">
        <div class="container">
            <div class="row">
                <div class="office__location__container">
                    <div class="header__content--sub-title office--sub--title">
                        <div class="header__divider"></div>
                        <div class="header__content--home--title">
                            <span>REACH OUT TO US</span>
                        </div>
                    </div>
                    <div class="contact__form__container--main--title--row">
                        <div class="contact__form__container--main--title"><h2><span>SyCip Gorres Velayo & Co.</span> <br/> Office Locations</h2></div>
                    </div>
                    <div class="office__location__container--office--row">
                        <div class="office__location__container--office--content">
                            <h3>Makati – Head Office</h3>
                            <div class="office__location__container--office--content--address">
                                <p>6760 Ayala Avenue, Makati City, <br/>
                                1226 Metro Manila, Philippines
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel: (632) 889-10307</p>
                                <p>Fax: (632) 819-0872 / (632) 818-1377</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Bacolod</h3>
                            <div class="office__location__container--office--content--address">
                                <p>No. 5 & 17 2nd floor MC Metroplex B.S. <br/>
                                Aquino Drive, Villamonte, Bacolod City, <br />
                                6100, Negros Occidental, Philippines
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel: (034) 441-2861 to 62</p>
                                <p>Fax: (034) 441-2863</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Baguio</h3>
                            <div class="office__location__container--office--content--address">
                                <p>3F Insular Life Bldg., <br/>
                                    Legarda Road corner Abanao St.,
                                    Baguio City 2600
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel: (074) 443-9858</p>
                                <p>Fax: (074) 442-6509</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Cagayan de Oro</h3>
                            <div class="office__location__container--office--content--address">
                                <p>Suites 4 & 5, Fourth Level<br />
                                    Gateway Tower 1, Limketkai Center <br />
                                    Lapasan, Cagayan de Oro City 
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel: (08822) 726-555,</p>
                                <p>(08822) 725-078</p>
                                <p>Fax: (088) 856-4415</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Cavite</h3>
                            <div class="office__location__container--office--content--address">
                                <p>Metrobank Rosario Branch Gen Trias Drive, <br />
Tejero Rosario, Cavite

                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel: (046) 437 7780 </p>
                                <p> (02) 845 2065 </p>
                                <p>Fax: (02) 741 1375</p>
                                <p>(046) 437 8059</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Cebu</h3>
                            <div class="office__location__container--office--content--address">
                                <p>Unit 1003 & 1004, Insular Life Cebu Business <br />
Centre Mindanao Avenue corner Biliran Road <br />
Cebu Business Park, Cebu City

                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel.: (032) 266-2947 to 49,</p>
                                <p>(032) 266-2821</p>
                                <p>(032) 266-2951</p>
                                <p>Fax: (032) 266-2313 </p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Clark</h3>
                            <div class="office__location__container--office--content--address">
                                <p>2nd Floor, 2 WorkPlus Building <br />
                                Mimosa Drive, Filinvest Mimosa Leisure City <br />
                                Clark Freeport Zone, Philippines, 2023
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                               <p>Mobile: (+63) 917-5656604</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Davao</h3>
                            <div class="office__location__container--office--content--address">
                                <p>5th floor Topaz Tower, Damosa IT Park <br/>
                        J. P Laurel Avenue, Lanang <br/>
                        8000 Davao City, Philippines
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                               <p>Tel: (082) 282-8447 to 51 </p>
                                <p>Fax: (082) 282-8438</p>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>General Santos</h3>
                            <div class="office__location__container--office--content--address">
                                <p>2F Elan 3 Building <br/>
                                    Roxas Avenue, Dadiangas East <br/>
                                    General Santos City <br/>
                                    9500 South Cotabato
                                </p>
                            </div>
                            <div class="office__location__container--office--content--contact">
                                <p>Tel: (083) 552-7001 to 02 </p>
                                <p>Fax: (083) 552-5314</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="news__publication">
        <div class="container">
            <div class="row">
                <div class="news__publication__container">
                    <div class="header__content--sub-title office--sub--title">
                        <div class="header__divider"></div>
                        <div class="header__content--home--title">
                            <span>LEADING THE WAY IN BUSINESS</span>
                        </div>
                    </div>
                    <div class="contact__form__container--main--title--row">
                        <div class="news__publication__container--main--title"><h2><span>Other SVG</span> <br/> News and Publications</h2></div>
                    </div>
                    <div class="news__publication__container--other--news">
                        <div class="news__publication__container--other--news--content">
                            <div class="news__publication__container--other--news--first--title">
                                <span>Browse All</span>
                            </div>
                            <div class="news__publication__container--other--news--second--title">
                                <span>
                                Corporate News
                                </span>
                                <span class="news__publication__container--other--news--icon">
                                    <img src="./assets/images/Material icons Negative-01-36@2x.png" alt=""/>
                                </span>
                            </div>
                        </div>
                        <div class="news__publication__container--other--news--content">
                            <div class="news__publication__container--other--news--first--title">
                                <span>Browse All</span>
                            </div>
                            <div class="news__publication__container--other--news--second--title">
                                <span>
                                C-Suite articles
                                </span>
                                <span class="news__publication__container--other--news--icon">
                                    <img src="./assets/images/Material icons Negative-01-36@2x.png" alt=""/>
                                </span>
                            </div>
                        </div>
                        <div class="news__publication__container--other--news--content">
                            <div class="news__publication__container--other--news--first--title">
                                <span>Browse All</span>
                            </div>
                            <div class="news__publication__container--other--news--second--title">
                                <span>
                                Publications
                                </span>
                                <span class="news__publication__container--other--news--icon">
                                    <img src="./assets/images/Material icons Negative-01-36@2x.png" alt=""/>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
