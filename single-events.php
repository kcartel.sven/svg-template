<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./assets/css/global-style.css">
<link rel="stylesheet" href="./assets/css/events-style.css">
<link rel="stylesheet" href="./assets/css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Signika&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

    <header class="main__header">
        <div class="header__overlay">
            <div class="container">
                <div class="row">
                    <div class="sample__nav">
                        <div class="sample__nav--first--container">
                        </div>
                        <div class="sample__nav--second--container">
                        </div>
                    </div>
                    
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>EVENTS & SEMINARS</span>
                            </div>
                        </div>
                        <div class="header__content--main--title--row">
                            <div class="header__content--main--title"><h1>PFRS Changes in 2019 and Beyond PRF 16, Leases Workshop</div>
                            <!-- <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row-divider"></div>

    <div class="single__events__container">
        <div class="container">
            <div class="row">
                <div class="single__events__container__row">
                    <div class="single__events__container__row--left">
                        <h3>Attendee Details</h3>
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>EVENTS &amp; SEMINARS</span>
                            </div>
                        </div>
                        <div class="single__events__container__row--left--forms">
                            <span class="single__events__container__row--input--row">
                                <span class="single__events__container__row--input--title">
                                    Firstname
                                </span>
                                <span class="single__events__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="single__events__container__row--input--row">
                                <span class="single__events__container__row--input--title">
                                Designation
                                </span>
                                <span class="single__events__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="single__events__container__row--input--row">
                                <span class="single__events__container__row--input--title">
                                CPA license no. / Expiry date
                                </span>
                                <span class="single__events__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <span class="single__events__container__row--input--row">
                                <span class="single__events__container__row--input--title">
                                    Email Address
                                </span>
                                <span class="single__events__container__row--input">
                                    <input type="text" class="input_form">
                                </span>
                            </span>
                            <div class="single__events__container__row--add-attendee" >
                                <div>
                                    <img src="./assets/images/add1.png" alt="">
                                </div>
                                <span>
                                ADD ATTENDEE

                                </span>
                            </div>
                        </div>
                        <div class="row-divider"></div>
                        <div>
                            <h3>Organization Details</h3>
                            <div class="single__events__container__row--left--forms">
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Organization 
Name
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    TIN
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Address
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Telephone No.
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Fax No.
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="row-divider"></div>
                        <div>
                            <h3>Contact Person</h3>
                            <div class="single__events__container__row--left--forms">
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Full Name
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Designation
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Email Address
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                                <span class="single__events__container__row--input--row">
                                    <span class="single__events__container__row--input--title">
                                    Contact No.
                                    </span>
                                    <span class="single__events__container__row--input">
                                        <input type="text" class="input_form">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="single__events__container__row--button--row">
                            <input type="button" class="send__message__button" value="Send Message"/>
                        </div>
                    </div>
                    <div class="single__events__container__row--right">
                        <p class="single__events__container__row--right--title">Download Brochure:</p>
                        <div class="single__events__container__row--button service__left--assurance--download--button">
                            <span class="single__events__container__row--download__pdf send__message__button">
                                Download PDF
                                <img src="./assets/images/chevron-right2.png" alt="">
                            </span>
                        </div>
                        <div class="single__events__container__row--right--content--seminar--two--column">
                            <div class="single__events__container__row--right--content--seminar">
                                <p class="single__events__container__row--right--content--seminar--title">Date:</p>
                                <p class="single__events__container__row--right--content--seminar--desc">October 23, 2020</p>
                            </div>
                            <div class="single__events__container__row--right--content--seminar">
                                <p class="single__events__container__row--right--content--seminar--title">Time:</p>
                                <p class="single__events__container__row--right--content--seminar--desc">1:00pm - 3:00 pm</p>
                            </div>
                        </div>
                        <div class="single__events__container__row--right--content--seminar">
                            <p class="single__events__container__row--right--content--seminar--title">Venue:</p>
                            <p class="single__events__container__row--right--content--seminar--desc">1st Floor, Ballroom 1, New World Hotel, Makati City</p>
                        </div>
                        <div class="single__events__container__row--right--content--seminar">
                            <p class="single__events__container__row--right--content--seminar--title">Topic:</p>
                            <p class="single__events__container__row--right--content--seminar--desc">1st PEZA Tax BaSe & </p>
                        </div>
                        <div class="single__events__container__row--right--content--seminar">
                            <p class="single__events__container__row--right--content--seminar--title">Description:</p>
                            <p class="single__events__container__row--right--content--seminar--desc">This half-day seminar will provide the participants a comprehensive 
                            discussion on the taxation of PEZA-registered entities, specifically the 
                            tax incentives being enjoyed by these entities. Moreover, this seminar 
                            will also present a side-by-side comparison of the taxation of PEZA-
                            registered entities during Income Tax Holiday (ITH) availment period and
                            during the 5% Gross Income Tax (GIT) regime and a discussion on 
                            proposed changes in Tax Reform Package 2 Corporate Income Tax and 
                            Incentives Rationalization Act (CITIRA) and Package 4 Passive Income 
                            and Financial Intermediary Taxation Act (PIFITA). In addition, this 
                            seminar will cover recent pronouncements/issuances from the Bureau 
                            of Internal Revenue (BIR) [i.e., Revenue Regulations (RRs), Revenue 
                            Memorandum Circulars (RMCs)]. Furthermore, relevant issuances from 
                            the Court, the PEZA and the Local Government Units (LGUs) applicable 
                            to PEZA-registered entities and other recent tax updates will also 
                            be discussed. </p>
                            <div class="single__events__container__row--right--content--seminar">
                                <p class="single__events__container__row--right--content--seminar--title">Speakers:</p>
                                <p class="single__events__container__row--right--content--seminar--desc">SGV Tax Partners and Principal who will be available to addres
any questions.</p>
                            </div>
                            <div class="single__events__container__row--right--content--seminar">
                                <p class="single__events__container__row--right--content--seminar--title">Fees:</p>
                                <p class="single__events__container__row--right--content--seminar--desc">PHP 6,720 per person (inclusive of VAT)<br/>
A 5% discount will be given to a group of three or more participants 
from the same company.<br/><br/>

For checks: Please make it payable to SyCip Gorres Velayo & Co<br/><br/>

For bank deposits: BPI – Ayala Branch; Account No. 1441-0040-75; <br/>
                                          Account Name: SyCip Gorres Velayo & Co</p>
                            </div>
                            <div class="single__events__container__row--right--content--seminar">
                                <p class="single__events__container__row--right--content--seminar--title">Cancellations & Transfers:</p>
                                <p class="single__events__container__row--right--content--seminar--desc">If you are unable to attend the seminar, a replacement is welcome to 
attend in your stead.  Otherwise, all cancellation requests must be
made in writing not later than two weeks before the seminar to qualify
for a refund</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="office__location event__seminar__single__container">
        <div class="container">
            <div class="row">
                <div class="office__location__container event__seminar__single__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title event__seminar__single__row--title">
                            <div class="contact__form__container--main--title"><h2><span>Other SGV Events</span> <br />and Seminars</h2></div>
                        </div>
                    </div>
                    <div class="office__location__container--office--row">
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Corporate Governance Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div class="office__location__container--office--content">
                            <h3>Risk Management Seminar</h3>
                            <div class="office__location__container--office--content--address">
                                <p>As an accredited institutional training 
                                    provider on Corporate Governance, SGV
                                    will be holding a seminar that will discuss
                                    Corporate Governance principles and
                                    leading practices.
                                </p>
                            </div>
                            <div class="events__container--office--content--address">
                                <div class="events__container--sub-title">
                                    <div class="events__container--sub--title-divider header__divider"></div>
                                    <div class="events__container--home--title">
                                        <span>MAR 24, 2020   1:00PM - 3:00PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="events__container--office--content--address">
                                <span class="events__container--register--button">
                                    Register now
                                    <img src="./assets/images/chevron-right1.png" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="single__events__container__row--paginate--row">
                        <div class="single__events__container__row--paginate--not--selected"></div>
                        <div class="single__events__container__row--paginate--selected"></div>
                        <div class="single__events__container__row--paginate--not--selected"></div>
                        <div class="single__events__container__row--paginate--not--selected"></div>
                    </div>
                    <div class="single__events__container__row--button service__left--assurance--download--button event__seminar__single__row--button">
                            <span class="single__events__container__row--download__pdf send__message__button">
                            View all SGV alumni news
                            </span>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="get__in__touch__container">
        <div class="container">
            <div class="row">
                <div class="get__in__touch__row single__events__get__in__touch__row">
                    <div class="header__content">
                        <div class="header__content--sub-title">
                            <div class="header__divider"></div>
                            <div class="header__content--home--title">
                                <span>HOME</span>
                            </div>
                        </div>
                        <div class="header__content--main--title">
                            <div class="contact__form__container--main--title"><h2><span>Get in</span> touch</h2></div>
                            <div class="header__content--description">As an accredited institutional training  provider on Corporate Governance, SGV will be holding a seminar that will discuss Corporate Governance principles and leading practices.</div>
                        </div>
                    </div>
                    <div class="event__get__in__touch--icon">
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-05-81.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                                <p class="event__get__in__touch--icon--title">Address</p>
                                <p>6760 Ayala Avenue, <br>
                                    Makati City, 1226 <br>
                                    Metro Manila, Philippines
                                </p>
                            </div>
                        </div>
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-05-11.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                            <p class="event__get__in__touch--icon--title">Phone</p>
                                <p>Tel: (632) 889-10307 <br>
                                    Fax: (632) 819-0872 / <br>
                                    (632) 818-1377
                                </p>
                            </div>
                        </div>
                        <div class="event__get__in__touch--icon--container">
                            <div class="event__get__in__touch--icon--image">
                                <img src="./assets/images/Material icons Negative-04-51.png" alt="">
                            </div>
                            <div class="event__get__in__touch--icon--description">
                                <p class="event__get__in__touch--icon--title">Email</p>
                                <p>info@sgv.ph
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
